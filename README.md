# Dracula At Dusk for [Visual Studio Code](http://code.visualstudio.com)

A fork of [Dracula at Night Official](https://github.com/bceskavich/dracula-at-night), with a pastel flavor.

![Screenshot](https://gitlab.com/Telokis/dracula-at-dusk/-/raw/master/demo.png)

## Install

1. Go to `View -> Command Palette` or press `⌘+shift+P`
2. Then enter `Install Extension`
3. Write `theme-dracula-at-dusk`
4. Select it or press Enter to install

## Contributing

If you'd like to contribute to this theme, please read the [contributing guidelines](./CONTRIBUTING.md).

## License

[MIT License](./LICENSE)
