# Changelog

## 1.0.6

- Added border color for sticky scroll block.
- Added shadow color for sticky scroll block.

## 1.0.5

- Made selection color brighter.
- Made comment color brighter.

## 1.0.4

- Made selection color darker.

## 1.0.3

- Changed inactive tab foreground color to be more visible.

## 1.0.2

- Increased contrast between selection and highlights.

## 1.0.1

- Changed colors so that green and cyan are more visible.
- Changed token colors fo that function declaration are in orange.
- Changed typescript types to green.

## 1.0.0

Initial release!

---

## Previous

This theme was forked from Dracula-at-night v2.6.0. See the Dracula-at-night Official [changelog](https://github.com/bceskavich/dracula-at-night/blob/master/CHANGELOG.md#2100) for changes preceding the fork.
