import { writeFileSync } from 'fs';
import { DraculaAtDusk } from './themes';

writeFileSync(
  `${__dirname}/../theme/dracula-at-dusk.json`,
  JSON.stringify(DraculaAtDusk, null, 2)
);
