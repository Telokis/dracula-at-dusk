import { Colors } from '../../Theme';
import TokenSettings from '../TokenSettings';

export default (colors: Colors): TokenSettings[] => [...constants(colors)];

function constants({ base }: Colors): TokenSettings[] {
  return [
    {
      name: 'JSON object keys',
      scope: [
        'meta.object-literal.key',
        'string.json support.type.property-name.json',
        'punctuation.support.type.property-name.begin.json',
        'punctuation.support.type.property-name.end.json',
      ],
      settings: {
        foreground: base.cyan,
      },
    },
  ];
}
